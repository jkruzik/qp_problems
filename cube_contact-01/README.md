# Permoncube contact
```
#subdomains                        8  (Nx Ny Nz)=(2 2 2)
#elements per subd.               64  (nx ny nz)=(4 4 4)
#all elements:                   512  (nx ny nz)=(8 8 8)
#coordinates:                    729
#DOFs undecomp.:                2187
#DOFs decomp.:                  3000
```

## Data notes
ce = ci = NULL

## Results
```
QPS Object: 8 MPI processes
  type: smalxe
  last QPSSolve CONVERGED due to CONVERGED_RTOL, KSPReason=2, required 12 iterations
  all 1 QPSSolves from last QPSReset/QPSResetStatistics have required 12 iterations
  tolerances: rtol=1.0e-06, abstol=1.0e-50, dtol=1.0e+50, maxits=100
  smalxe specific:
    Total number of inner iterations 72
    #hits    of M1, eta:   3,   2
    #updates of M1, rho:   3,   7
    inner     QPS Object: (smalxe_) 8 MPI processes
      type: mpgp
      last QPSSolve CONVERGED due to CONVERGED_HAPPY_BREAKDOWN, KSPReason=7, required 1 iterations
      all 12 QPSSolves from last QPSReset/QPSResetStatistics have required 72 iterations
      tolerances: rtol=1.0e-05, abstol=6.2e-12, dtol=1.0e+50, maxits=10000
      mpgp specific:
        from the last QPSReset:
        number of Hessian multiplications 89
        number of CG steps 66
        number of expansion steps 5
        number of proportioning steps 1
=====================
QP Object: QP_0x84000004_2 (dual_prpnlt_) 8 MPI processes
  type: QP
  #7 in chain, derived by QPTEnforceEqByPenalty
|| x|| = 4.15995969e+01    max( x) = 4.18e+00 =  x(816)    min( x) = -4.97e+00 =  x(765)    0x55fc9360ce70
|| b|| = 6.96552042e-02    max( b) = 7.95e-03 =  b(74)    min( b) = -1.06e-02 =  b(498)    0x55fc9366ec70
r = ||A*x - b - lambda_lb|| = 0.00e+00    rO/||b|| = 0.00e+00
r = ||min(x-lb,0)||      = 0.00e+00    r/||b|| = 0.00e+00
r = ||min(lambda_lb,0)|| = 4.06e-08    r/||b|| = 5.82e-07
r = |lambda_lb'*(lb-x)|  = 1.67e-07    r/||b|| = 2.39e-06
-------------------
QP Object: QP_0x84000004_3 (dual_proj_) 8 MPI processes
  type: QP
  #6 in chain, derived by QPTEnforceEqByProjector
|| x|| = 4.15995969e+01    max( x) = 4.18e+00 =  x(816)    min( x) = -4.97e+00 =  x(765)    0x55fc9360ce70
|| b|| = 6.96526149e-02    max( b) = 7.95e-03 =  b(74)    min( b) = -1.06e-02 =  b(935)    0x55fc9356fe80
||cE|| = 0.00e-00    max(cE) = 0.00e-00 = cE(0)    min(cE) = 0.00e-00 = cE(0)
r = ||A*x - b + (B'*lambda) - lambda_lb|| = 4.38e-08    rO/||b|| = 6.29e-07
r = ||BE*x||             = 6.44e-08    r/||b|| = 9.25e-07
r = ||min(x-lb,0)||      = 0.00e+00    r/||b|| = 0.00e+00
r = ||min(lambda_lb,0)|| = 4.06e-08    r/||b|| = 5.82e-07
r = |lambda_lb'*(lb-x)|  = 1.67e-07    r/||b|| = 2.39e-06
-------------------
QP Object: QP_0x84000004_4 (dual_) 8 MPI processes
  type: QP
  #5 in chain, derived by QPTHomogenizeEq
|| x|| = 4.15995969e+01    max( x) = 4.18e+00 =  x(816)    min( x) = -4.97e+00 =  x(765)    0x55fc93592b30
|| b|| = 3.02911096e-01    max( b) = 2.65e-02 =  b(264)    min( b) = -3.64e-02 =  b(498)    0x55fc935866b0
||cE|| = 0.00e-00    max(cE) = 0.00e-00 = cE(0)    min(cE) = 0.00e-00 = cE(0)
r = ||A*x - b + (B'*lambda) - lambda_lb|| = 0.00e+00    rO/||b|| = 0.00e+00
r = ||BE*x||             = 6.44e-08    r/||b|| = 2.13e-07
r = ||min(x-lb,0)||      = 0.00e+00    r/||b|| = 0.00e+00
r = ||min(lambda_lb,0)|| = 2.83e-08    r/||b|| = 9.34e-08
r = |lambda_lb'*(lb-x)|  = 5.41e-08    r/||b|| = 1.79e-07
-------------------
QP Object: QP_0x84000004_5 (dual_) 8 MPI processes
  type: QP
  #4 in chain, derived by QPTOrthonormalizeEq
|| x|| = 9.06491945e+01    max( x) = 1.08e+01 =  x(816)    min( x) = -7.72e+00 =  x(430)    0x55fc92b9f060
|| b|| = 8.68717258e+00    max( b) = 1.01e+00 =  b(775)    min( b) = -8.21e-01 =  b(1212)    0x55fc92f38700
||cE|| = 2.07954322e+01    max(cE) = 0.00e+00 = cE(0)    min(cE) = -1.04e+01 = cE(26)    0x55fc92ebb390
r = ||A*x - b + (B'*lambda) - lambda_lb|| = 2.48e-25    rO/||b|| = 2.85e-26
r = ||BE*x-cE||         not available
r = ||min(x-lb,0)||      = 0.00e+00    r/||b|| = 0.00e+00
r = ||min(lambda_lb,0)|| = 2.83e-08    r/||b|| = 3.26e-09
r = |lambda_lb'*(lb-x)|  = 5.41e-08    r/||b|| = 6.23e-09
-------------------
QP Object: QP_0x84000004_6 (dual_) 8 MPI processes
  type: QP
  #3 in chain, derived by QPTScale
|| x|| = 9.06491945e+01    max( x) = 1.08e+01 =  x(816)    min( x) = -7.72e+00 =  x(430)    0x55fc92b9f060
|| b|| = 8.68717258e+00    max( b) = 1.01e+00 =  b(775)    min( b) = -8.21e-01 =  b(1212)    0x55fc92f38700
||cE|| = 2.07954322e+01    max(cE) = 0.00e+00 = cE(0)    min(cE) = -1.04e+01 = cE(26)    0x55fc92ebb390
r = ||A*x - b + (B'*lambda) - lambda_lb|| = 0.00e+00    rO/||b|| = 0.00e+00
r = ||BE*x-cE||          = 3.74e-08    r/||b|| = 4.31e-09
r = ||min(x-lb,0)||      = 0.00e+00    r/||b|| = 0.00e+00
r = ||min(lambda_lb,0)|| = 2.83e-08    r/||b|| = 3.26e-09
r = |lambda_lb'*(lb-x)|  = 5.41e-08    r/||b|| = 6.23e-09
-------------------
QP Object: QP_0x84000004_7 (dual_) 8 MPI processes
  type: QP
  #2 in chain, derived by QPTDualize
|| x|| = 9.06491945e+01    max( x) = 1.08e+01 =  x(816)    min( x) = -7.72e+00 =  x(430)    0x55fc92b9f060
|| b|| = 8.68717258e+00    max( b) = 1.01e+00 =  b(775)    min( b) = -8.21e-01 =  b(1212)    0x55fc92f38700
||cE|| = 2.07954322e+01    max(cE) = 0.00e+00 = cE(0)    min(cE) = -1.04e+01 = cE(26)    0x55fc92ebb390
r = ||A*x - b + (B'*lambda) - lambda_lb|| = 0.00e+00    rO/||b|| = 0.00e+00
r = ||BE*x-cE||          = 3.74e-08    r/||b|| = 4.31e-09
r = ||min(x-lb,0)||      = 0.00e+00    r/||b|| = 0.00e+00
r = ||min(lambda_lb,0)|| = 2.83e-08    r/||b|| = 3.26e-09
r = |lambda_lb'*(lb-x)|  = 5.41e-08    r/||b|| = 6.23e-09
-------------------
QP Object: QP_0x84000004_8 8 MPI processes
  type: QP
  #1 in chain, derived by QPTScale
|| x|| = 4.18018062e-01    max( x) = 4.07e-03 =  x(1495)    min( x) = -2.35e-02 =  x(2612)    0x55fc92afe810
|| b|| = 5.08593750e+01    max( b) = 0.00e+00 =  b(0)    min( b) = -7.27e+00 =  b(1820)    0x55fc92af8900
||cE|| = 0.00e-00    max(cE) = 0.00e-00 = cE(0)    min(cE) = 0.00e-00 = cE(0)
||cI|| = 0.00e-00    max(cI) = 0.00e-00 = cI(0)    min(cI) = 0.00e-00 = cI(0)
r = ||A*x - b + B'*lambda|| = 2.48e-07    rO/||b|| = 4.88e-09
r = ||BE*x||             = 3.77e-08    r/||b|| = 7.40e-10
r = ||max(BI*x,0)||      = 4.23e-09    r/||b|| = 8.31e-11
r = ||min(lambda_I,0)||  = 0.00e+00    r/||b|| = 0.00e+00
r = |lambda_I'*(BI*x)|= 3.60e-08       r/||b|| = 7.08e-10
-------------------
QP Object: QP_0x84000004_9 8 MPI processes
  type: QP
  #0 in chain, derived by
|| x|| = 4.18018062e-01    max( x) = 4.07e-03 =  x(1495)    min( x) = -2.35e-02 =  x(2612)    0x55fc92afe810
|| b|| = 5.08593750e+01    max( b) = 0.00e+00 =  b(0)    min( b) = -7.27e+00 =  b(1820)    0x55fc92af8900
||cE|| = 0.00e-00    max(cE) = 0.00e-00 = cE(0)    min(cE) = 0.00e-00 = cE(0)
||cI|| = 0.00e-00    max(cI) = 0.00e-00 = cI(0)    min(cI) = 0.00e-00 = cI(0)
r = ||A*x - b + B'*lambda|| = 2.48e-07    rO/||b|| = 4.88e-09
r = ||BE*x||             = 3.77e-08    r/||b|| = 7.40e-10
r = ||max(BI*x,0)||      = 4.23e-09    r/||b|| = 8.31e-11
r = ||min(lambda_I,0)||  = 0.00e+00    r/||b|| = 0.00e+00
r = |lambda_I'*(BI*x)|= 3.60e-08       r/||b|| = 7.08e-10
=====================
```

## Run
```bash
mpir -n 8  ./maint-llvm-dbg/permoncube  -x4 -y4 -z4 -X2 -Y2 -Z2 -c -@  -qps_view_convergence -feti -qps_rtol 1e-6 -dual_qp_E_orth_type implicit -dual_qp_E_orth_form implicit -qp_R_orth_type gs -dual_mat_inv_pc_factor_mat_solver_type mumps -dual_mat_inv_pc_type cholesky -dual_pc_dual_type none -qps_ksp_norm_type unpreconditioned -feti_gluing_type orth -qps_view
```

## Relevant parameters
```
rtol        = 1e-6
M1_user     = 10
M1_update   = 10
rho_user    = 1.1
rho_update  = 1.0
eta_user    = 1e-1
```

## Software
PERMON 3.19.1

PETSc 3.19.0

